<?php
/**
 * @file
 * Administrative functionality for the Holiday Notifier module.
 */

/**
 * Form constructor for the module settings page.
 *
 * @see holiday_notifier_settings_form_submit()
 */
function holiday_notifier_settings_form($form, &$form_state) {
  // Check Date_Holidays installation availability first
  if (!_holiday_notifier_check_date_holidays()) {
    return;
  }

  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Import holidays'),
  );

  $countries = _holiday_notifier_get_drivers();

  // Do not show the form if an error occurred while getting the drivers
  if (is_null($countries)) {
    return;
  }

  $form['country'] = array(
    '#type' => 'select',
    '#title' => t('Country/driver'),
    '#options' => $countries,
    '#required' => TRUE
  );

  $form['year'] = array(
    '#type' => 'date_select',
    '#title' => t('Year'),
    '#date_format' => 'Y',
    '#date_year_range' => '0:+10',
    '#default_value' => date("Y"),
    '#date_label_position' => 'none',
    '#required' => TRUE
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 * Private helper function to verify the Date_Holidays installation.
 *
 * Checks that the Holidays.php class is available from the Date_Holidays PEAR
 * package. Primarily tries to use the class from a system-wide PEAR installation.
 * Secondarily tries a local PEAR installation (Date_Holidays, its drivers and
 * all dependencies installed in "pear" directory under the module directory).
 * If neither one works, an error is displayed to the user.
 *
 * @return boolean
 *   TRUE, if Date_Holidays is available, otherwise FALSE.
 *
 * @see holiday_notifier_settings_form()
 */
function _holiday_notifier_check_date_holidays() {
  // Try from system-wide installation
  if (@include_once("Date/Holidays.php")) {
    return TRUE;
  }

  // Try from local installation
  $local_pear_path = drupal_get_path('module', 'holiday_notifier') . '/pear/php';
  $set_path = set_include_path(get_include_path() . PATH_SEPARATOR . $local_pear_path);

  if ($set_path && @include_once("Date/Holidays.php")) {
    return TRUE;
  }

  drupal_set_message(t('PEAR package Date_Holidays installation was not found in
    your system (in your PHP include path) and thus holiday information cannot
    be automatically imported.
    <br/><br/>The recommended way to enable the package is to have it installed
    system-wide by the server administrator along with desired Date_Holidays
    drivers for different countries and holidays.
    <br/><br/>Alternatively, a local PEAR installation is also supported.
    Create a PEAR installation (with Date_Holidays, desired drivers and required
    dependencies) using <a href="http://pear2.php.net/PEAR2_Pyrus">Pyrus</a>,
    and copy the installation into a directory named "pear" under the
    Holiday Notifier module directory.
    In this case the Holidays.php class must be found in the
    following path under your Drupal installation root: %holidays_path',
    array('%holidays_path' => $local_pear_path . '/Date/Holidays.php')), 'error');

  return FALSE;
}

/**
 * Private helper function to get the available PEAR Date_Holidays drivers.
 *
 * @return array
 *   An array of available Date_Holidays drivers (driver ids as keys, driver
 *   titles as values) to be used in a select list on the module settings page.
 *   In error situations NULL is returned.
 *
 * @see holiday_notifier_settings_form()
 */
function _holiday_notifier_get_drivers() {
  $dh = new Date_Holidays();
  $drivers = $dh->getInstalledDrivers();
  $driver_ids = array();
  $driver_titles = array();

  foreach ($drivers as $d) {
    $loaded_driver = $dh->factory($d['id'], NULL, 'en_EN');

    if ($dh->isError($loaded_driver)) {
      drupal_set_message(t('Loading a Date_Holidays driver (id: %id, title: %title)
        failed! Please check your Date_Holidays PEAR installation.',
        array('%id' => $d['id'], '%title' => $d['title'])), 'error');

      return NULL;
    }

    $holidays = $loaded_driver->getHolidays(NULL, 'en_EN');

    if ($dh->isError($holidays)) {
      drupal_set_message(t('Loading holiday information for a Date_Holidays driver
        (id: %id, title: %title) failed! Please check your Date_Holidays PEAR
        installation.',
        array('%id' => $d['id'], '%title' => $d['title'])), 'error');
      return NULL;
    }

    // Exclude drivers which do not offer any real holiday information
    if (count($holidays) == 0) {
      continue;
    }

    $driver_ids[] = $d['id'];
    $driver_titles[] = $d['title'];
  }

  return array_combine($driver_ids, $driver_titles);
}

/**
 * Form submission handler for holiday_notifier_settings_form().
 */
function holiday_notifier_settings_form_submit($form, &$form_state) {
  $dh = new Date_Holidays();
  $driver = $dh->factory($form_state['values']['country'],
    $form_state['values']['year'],
    'en_EN');

  if ($dh->isError($driver)) {
    drupal_set_message(t('Loading the Date_Holidays driver (id: %id) failed!
      Please check your Date_Holidays PEAR installation.',
      array('%id' => $form_state['values']['country'])), 'error');
    return;
  }

  $country_codes = $driver->getISO3166Codes();
  $country_code = NULL;

  if (!($dh->isError($country_codes))) {
    // Validate ISO 3166-1 alpha-2 code with the help of the Countries module if
    // available
    if (function_exists('country_load')) {
      foreach ($country_codes as $code) {
        if (drupal_strlen($code) == 2 && country_load($code) != FALSE) {
          $country_code = drupal_strtoupper($code);
          break;
        }
      }
    }
    // Otherwise just rely on the given code and hope it's decent
    else {
      foreach ($country_codes as $code) {
        if (drupal_strlen($code) == 2) {
          $country_code = drupal_strtoupper($code);
          break;
        }
      }
    }
  }

  if ($country_code == NULL) {
    drupal_set_message(t('The Date_Holidays driver selected (id: %id) does not
      provide a valid ISO 3166-1 alpha-2 country code identifier. Please edit
      all the Holiday nodes manually and set the proper country.',
      array('%id' => $form_state['values']['country'])), 'warning');
  }

  $holidays = $driver->getHolidays(NULL, 'en_EN');

  if ($dh->isError($holidays)) {
    drupal_set_message(t('Loading holiday information for the Date_Holidays driver
      (id: %id) failed! Please check your Date_Holidays PEAR installation.',
      array('%id' => $form_state['values']['country'])), 'error');
    return;
  }

  $holidays_added = '<ul>';

  foreach ($holidays as $holiday) {
    $node = new stdClass();
    $node->title = $holiday->getTitle();
    $node->language = LANGUAGE_NONE;
    $node->type = "holiday";

    node_object_prepare($node);

    // @todo 'All day' is not reliably set like this. See http://drupal.org/node/874322
    // for discussion about the problem.
    $node->field_date['und'][0]['value'] = $holiday->getDate()->getDate();
    $node->field_date['und'][0]['value2'] = $holiday->getDate()->getDate();

    if ($country_code != NULL) {
      $node->field_countries['und'][0]['iso2'] = $country_code;
    }

    $node = node_submit($node);
    node_save($node);

    // Add to the message shown to user
    $date = $holiday->getDate();
    $year = $date->getYear();
    $month = $date->getMonth();
    $day = $date->getDay();
    $holidays_added .= '<li>' . $holiday->getTitle() . " ($year-$month-$day)</li>";
  }

  $holidays_added .= '</ul>';

  drupal_set_message(t('The following Holiday nodes were successfully added:!list',
    array('!list' => filter_xss($holidays_added))));
}

/**
 * Page callback: Displays module API demonstration page.
 *
 * @return string
 *   HTML to be rendered.
 *
 * @see holiday_notifier_menu()
 */
function holiday_notifier_api_demo_page() {
  $markup = '<h1>Simple custom format (2013-05-01, no country filters):</h1>' .
    '<h3>function call: holiday_notifier_get_holidays("2013-05-01")</h3>' .
    '<pre>' . print_r(holiday_notifier_get_holidays("2013-05-01"), TRUE) .
    '</pre>' .
    '<h1>Full node format (2013-05-01, no country filters):</h1>' .
    '<h3>function call: holiday_notifier_get_holidays("2013-05-01", NULL, TRUE)</h3>' .
    '<pre>' . print_r(holiday_notifier_get_holidays("2013-05-01", NULL, TRUE), TRUE) .
    '</pre>' .
    '<h1>Simple custom format (2013-05-01, countries: RU, SE):</h1>' .
    '<h3>function call: holiday_notifier_get_holidays("2013-05-01", array("RU", "SE"))</h3>' .
    '<pre>' . print_r(holiday_notifier_get_holidays("2013-05-01", array("RU", "SE")), TRUE) .
    '</pre>' .
    '<h1>Full node format (2013-05-01, countries: RU, SE):</h1>' .
    '<h3>function call: holiday_notifier_get_holidays("2013-05-01", array("RU", "SE"), TRUE)</h3>' .
    '<pre>' . print_r(holiday_notifier_get_holidays("2013-05-01", array("RU", "SE"), TRUE), TRUE) .
    '</pre>';

  return $markup;
}

