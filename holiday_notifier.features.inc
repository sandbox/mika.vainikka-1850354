<?php
/**
 * @file
 * holiday_notifier.features.inc
 */

/**
 * Implements hook_views_api().
 */
function holiday_notifier_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function holiday_notifier_node_info() {
  $items = array(
    'holiday' => array(
      'name' => t('Holiday'),
      'base' => 'node_content',
      'description' => t('Use <em>holiday</em> to display holidays for various countries.'),
      'has_title' => '1',
      'title_label' => t('Holiday'),
      'help' => '',
    ),
  );
  return $items;
}

