-- SUMMARY --

Holiday Notifier module can be used to display holidays or other special days
for selected countries. Default blocks include a calendar view with country
selection and a notification view that shows upcoming holidays in a list.

Administrator can edit, add and delete holiday information through the custom
Holiday content type. However, a more advanced way of providing holiday
information is enabled through using the Date_Holidays PEAR package and its
drivers. 


-- REQUIREMENTS --

Holiday Notifier works with Drupal 7.x. Moreover, this module has dependencies
to other Drupal modules such as Calendar, Countries, Date, Entity, Features,
Field UI, List and Views UI.

In addition to mentioned Drupal modules this module is also optionally dependent
on the Date_Holidays PEAR package. This component is used to automatically
calculate holidays occurring in different countries together with country
specific drivers that will offer specific nation based calculation rules.

Date_Holidays requirement is optional: you can use the module with manually
added holiday information alone, Date_Holidays and its drivers are only needed
for automated import functionality.

See http://pear.php.net/package/Date_Holidays/ for details about Date_Holidays,
and INSTALLATION in this document for more information.


-- INSTALLATION --

Place the uncompressed module directory under the modules directory within your
Drupal installation. See that you have all the dependency modules installed, and
enable Holiday Notifier on the modules page.

Holiday Notifier is a feature and thus making use of the Features module. Please
see the Features module documentation for how to deal with feature management.

If you want to use the automated holiday import functionality, you need to have
the Date_Holidays PEAR package and the selected drivers for it installed on your
server. Date_Holidays must be available in PHP's include path, you have two
alternatives:

1) System-wide PEAR installation (recommended).
2) Local PEAR installation within the module directory.

In option 1) Date_Holidays is used from a system-wide PEAR installation. Ask
your system administrator to install the required packages for you.

In option 2) you will have to create a local PEAR installation by yourself to be
used by the module. Create the installation using the Pyrus tool
(http://pear2.php.net/PEAR2_Pyrus), and include Date_Holidays with all the
dependencies and the country drivers you want. Then, copy the installation into
a directory named "pear" under the Holiday Notifier module directory.

In this case the full path to the Holidays.php class should be something
like "sites/all/modules/holiday_notifier/pear/php/Date/Holidays.php". Holiday
Notifier will then automatically try to include the required classes.

See the module configuration page for the Date_Holidays installation status:
if the package is available, you should be able to select countries/drivers from
a select menu. Otherwise an error message will be shown.

